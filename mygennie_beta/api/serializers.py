from rest_framework import serializers
from .models import CustomerCredentials,CustomerDetails,SellerCredentials,SellerDetails,FoodDetails,FoodCatalogue,LaundryOrders,BakeryOrders,FoodOrders

class CustomerCredentialsSerializer(serializers.ModelSerializer):
	class Meta:
		model = CustomerCredentials
		fields = ('c_id','email','password')


class CustomerDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model = CustomerDetails
		fields = ('c_id','first_name','last_name','email','hostel_no','room_no ','quater_no','phone_no','DOB','registration_date')


class SellerCredentialsSerializer(serializers.ModelSerializer):
	class Meta:
		model = SellerCredentials
		fields = ('s_id','email','password')


class SellerDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model = SellerDetails
		fields = ('s_id','first_name','last_name','email','phone_no','DOB','contract_no','address','registration_date')


class FoodDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model = FoodDetails
		fields = ('food_id','seller_id','unit_price','name','description','image_url')


class FoodCatalogueSerializer(serializers.ModelSerializer):
	class Meta:
		model = FoodCatalogue
		fields = ('food_id','gen_id','unit_price','name','description','thumbnail_url','image_url')


class FoodOrdersSerializer(serializers.ModelSerializer):
	class Meta:
		model = FoodOrders
		fields = ('forder','cart_id','food_id','quantity','order_status')

class LaundryOrdersSerializer(serializers.ModelSerializer):
	class Meta:
		model = LaundryOrders
		fields = ('order_id','customer_id')


class BakeryOrdersSerializer(serializers.ModelSerializer):
	class Meta:
		model = BakeryOrders
		fields = ('order_id','customer_id','greeting')