from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import CustomerCredentials,CustomerDetails,SellerCredentials,SellerDetails,FoodDetails,FoodCatalogue,LaundryOrders,BakeryOrders,FoodOrders,CustomerToken,FoodCart
from .serializers import CustomerCredentialsSerializer,CustomerDetailsSerializer,SellerCredentialsSerializer,SellerDetailsSerializer,FoodDetailsSerializer,FoodCatalogueSerializer,LaundryOrdersSerializer,BakeryOrdersSerializer,FoodOrdersSerializer
import re,base64,hashlib,time,json

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data,headers):
        content = JSONRenderer().render(data)
        super(JSONResponse, self).__init__(content)
        self['Content-Type'] = 'application/json'
        for key,value in headers.iteritems():
            self[str(key)] = value


@csrf_exempt
def prelogin(request):
    if request.method == 'POST':
        user_id = request.META['HTTP_USERID']
        try:
            user = CustomerCredentials.objects.get(email=user_id)
            time_str = str(time.time())
            nonce = hashlib.sha224(user_id + time_str).hexdigest()
            headers = {}
            headers['nonce'] = nonce
            headers['Access-Control-Expose-Headers'] = 'nonce'
            return JSONResponse({'ok' : 'user'},headers)
        except CustomerCredentials.DoesNotExist:
            return JSONResponse({'error':'User Doesnot exist'},{})
    else:
        return JSONResponse({'error' : 'Invalid method'},{})


@csrf_exempt
def login(request):
    if request.method == 'POST':
        credentials = request.META['HTTP_AUTHORIZATION']
        nonce = request.META['HTTP_NONCE']
        data = str(credentials)
        data = data.replace('Basic ','')
        data = base64.b64decode(data)
        reg=re.compile('(\w+)[:=] ?"?(\w+)"?')
        data = dict(reg.findall(data))
        for key in data:
            username = key
        password = data[username]
        lenght = len(username)
        if (lenght % 4) == 2 :
            username = username + '=='
        elif (lenght % 4) == 3 :
            username = username + '='
        else :
            username = username
        username = base64.b64decode(username)
        try:
            user = CustomerCredentials.objects.get(email=username)
            passwd = str(user.password)
            token = passwd + nonce
            token = hashlib.sha224(token).hexdigest()
            if token == password:
                time_str = str(time.time())
                headers = {}
                token = CustomerToken()
                token.user_id = user
                token.actions = '1,'
                token.save()
                headers['token'] = token.token
                headers['Access-Control-Expose-Headers'] = 'token'
                return JSONResponse({'ok':'good'},headers)
            else:
                return JSONResponse({'error':'Invalid Credentials'},{})
        except CustomerCredentials.DoesNotExist:
            return JSONResponse({'error':'Invalid User'},{})
    else:
        return JSONResponse({'error' : 'Invalid method'},{})        


@csrf_exempt
def logout(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        try:
            tokendata = CustomerToken.objects.get(token=token)
            actions = tokendata.actions + ',10'
            CustomerToken.objects.filter(token=token).update(actions=actions)
            return JSONResponse({'ok':'done'},{})
        except CustomerToken.DoesNotExist:
            return JSONResponse({'error':'Invalid Token'},{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})


# Provides whole cart details
@csrf_exempt
def cart(request,cart_id):
    if request.method == 'GET':
        token = request.META['HTTP_TOKEN']
        try:
            tokendata = CustomerToken.objects.get(token=token)
            try:
                cartdetails = FoodOrders.objects.all().filter(cart_id=cart_id)
                serializer = FoodOrdersSerializer(cartdetails,many=True)
                return JSONResponse(serializer.data,{})
            except FoodOrders.DoesNotExist:
                return JSONResponse({'error':'Invalid Cart'},{})
        except CustomerToken.DoesNotExist:
            return JSONResponse({'error':'Invalid Token'},{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})


# Provide individual order details
@csrf_exempt
def orderdetails(request,order_id):
    if request.method == 'GET':
        token = request.META['HTTP_TOKEN']
        try:
            tokendata = CustomerToken.objects.get(token=token)
            order_id = int(order_id)
            try:
                orderdetail = FoodOrders.objects.all().filter(forder=order_id)
                serializer =   FoodOrdersSerializer(orderdetail,many=True)
                return JSONResponse(serializer.data,{})
            except FoodOrders.DoesNotExist:
                return JSONResponse({'error':'Invalid Order Id'},{})
        except CustomerToken.DoesNotExist:
            return JSONResponse({'error':'Invalid Token'},{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})


# Provide catalogue
@csrf_exempt
def catalogue(request):
    if request.method =='GET':
        token = request.META['HTTP_TOKEN']
        try:
            tokandata = CustomerToken.objects.get(token=token)
            foodlist = FoodCatalogue.objects.all()
            serializer = FoodCatalogueSerializer(foodlist,many=True)
            return JSONResponse(serializer.data,{})
        except CustomerToken.DoesNotExist:
            return JSONResponse({'error':'Invalid Token'},{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})

# Placing order
@csrf_exempt
def placeorder(request):
    if request.method =='PUT':
        token = request.META['HTTP_TOKEN']
        try:
            tokendata = CustomerToken.objects.get(token=token)
            data = request.body
            data = data.replace('\n','')
            data = data.replace('\"','"')
            data = json.loads(data)
            user = tokendata.user_id
            if data['address'] == '0':
                customer = CustomerDetails.objects.get(pk=user)
                if customer.quater_no == '0':
                    address = 'Room No : ' + customer.room_no + ' , Hostel no : ' + customer.hostel_no + ' '
                else:
                    address = 'Quater No : ' + customer.quater_no + ' .'
            else:
                address = data['address']
            orderitems = data['items']
            bill = 0    
            for items in orderitems:
                food_id = int(items['food_id'])
                food = FoodDetails.objects.get(pk=food_id)
                price = food.unit_price
                quantity = int(items['quantity'])
                bill = bill + ( price * quantity )
            cart = FoodCart()
            cart.customer = user
            cart.delivery_address = address
            cart.bill = bill
            cart.save()
            for items in orderitems:
                foodorder = FoodOrders()
                foodorder.cart_id = cart
                food = FoodDetails.objects.get(pk=int(items['food_id']))
                foodorder.food_id = food
                foodorder.quantity = int(items['quantity'])
                foodorder.order_status = '0'
                foodorder.save()
            placed_orders = FoodOrders.objects.all().filter(cart_id=cart.cart)
            serializer = FoodOrdersSerializer(placed_orders,many=True)
            orders = {}
            orders['list'] = serializer.data
            orders['bill'] = str(bill)
            return JSONResponse(orders,{})
        except CustomerToken.DoesNotExist:
            return JSONResponse({'error':'Invalid Token'},{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})
















@csrf_exempt
def json_test(request):
    if request.method == 'PUT':
        data = request.body
        data = data.replace('\"','"')
        data = data.replace("\n","")
        data = json.loads(data)
        return JSONResponse(data,{})
    else:
        return JSONResponse({'error':'Invalid Method'},{})






@csrf_exempt
def customer_list(request):

    if request.method == 'GET':
        customers = CustomerCredentials.objects.all()
        serializer = CustomerCredentialsSerializer(customers, many=True)
        var = {}
        var = request.META['HTTP_AUTHORIZATION']
        reg=re.compile('(\w+)[:=] ?"?(\w+)"?')
        var2 = dict(reg.findall(var))
        user = str(var2['username'])
        password = str(var2['response'])
        try:
            test_customer = CustomerCredentials.objects.get(email='paul.bishalpaul.bishal@gmail.com')                        
            if str(test_customer.password) == password :
                return JSONResponse(serializer.data,var2)
            else:
                return JSONResponse({},{'error' : 'Invalid Password'})
        except CustomerCredentials.DoesNotExist:
            return JSONResponse({},{'error' : 'User doesnot Exist'})               
#        

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CustomerCredentialsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def catalogue(request):
    if request.method == 'GET':
        foods = FoodCatalogue.objects.all()
        serializer = FoodCatalogueSerializer(foods,many=True)
        return JSONResponse(serializer.data,{})
    else:
        return JSONResponse({'error' : 'Invalid method'},{})
