from django.contrib import admin

# Register your models here.
from .models import CustomerCredentials,CustomerDetails,SellerCredentials,SellerDetails,FoodDetails,FoodCatalogue,LaundryOrders,BakeryOrders,FoodOrders,CustomerToken,FoodCart


admin.site.register(CustomerCredentials)
admin.site.register(CustomerDetails)
admin.site.register(SellerCredentials)
admin.site.register(SellerDetails)
admin.site.register(FoodDetails)
admin.site.register(FoodCatalogue)
admin.site.register(FoodOrders)
admin.site.register(LaundryOrders)
admin.site.register(BakeryOrders)
admin.site.register(FoodCart)



