from __future__ import unicode_literals

from django.db import models

import base64,hashlib,time
# Create your models here.

#Customer Credentials
class CustomerCredentials(models.Model):
	c_id = models.AutoField(primary_key=True,unique=True)
	email = models.EmailField(max_length=50)
	password = models.CharField(max_length=60)
#	access_token = models.CharField(max_length=10)

	def save(self, *args, **kwargs):
		self.password = hashlib.sha224(self.password).hexdigest()
#		self.access_token = base64.b64encode(self.c_id)
		super(CustomerCredentials, self).save(*args, **kwargs)

	def __str__(self):
		return self.email


#Customer Details
class CustomerDetails(models.Model):
	c_id = models.ForeignKey(CustomerCredentials,primary_key=True,unique=True,on_delete=models.CASCADE)
	first_name = models.CharField(max_length=15)
	last_name = models.CharField(max_length=15)
	email = models.EmailField(max_length=50)
	hostel_no = models.CharField(max_length=2)
	room_no = models.CharField(max_length=6)
	quater_no = models.CharField(max_length=3)
	phone_no = models.CharField(max_length=10)
	DOB = models.DateField(auto_now=False)
	registration_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.first_name + ' ' + self.last_name



#Seller Credentials
class SellerCredentials(models.Model):
	s_id = models.AutoField(primary_key=True,unique=True)
	email = models.EmailField(max_length=50)
	password = models.CharField(max_length=60)
#	access_token = models.CharField(max_length=10)

	def save(self, *args, **kwargs):
		self.password = hashlib.sha224(self.password).hexdigest()
#		self.access_token = base64.b64encode(self.c_id)
		super(SellerCredentials, self).save(*args, **kwargs)

	def __str__(self):
		return self.email

#Seller Details
class SellerDetails(models.Model):
	c_id = models.ForeignKey(SellerCredentials,primary_key=True,unique=True,on_delete=models.CASCADE)
	first_name = models.CharField(max_length=15)
	last_name = models.CharField(max_length=15)
	email = models.EmailField(max_length=50)
	phone_no = models.CharField(max_length=10)
	contract_no = models.CharField(max_length=5)
	address = models.CharField(max_length=100)
	DOB = models.DateField(auto_now=False)
	registration_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.first_name + ' ' + self.last_name



# Food Details
class FoodDetails(models.Model):
	food_id = models.AutoField(primary_key=True)
	seller_id = models.ForeignKey(SellerCredentials)
	unit_price = models.PositiveSmallIntegerField()
	name = models.CharField(max_length=20)
	description = models.CharField(max_length=100)
	image_url = models.URLField(max_length=50)

	def __str__(self):
		return self.name

# Catalogue
class FoodCatalogue(models.Model):
	food_id = models.ForeignKey(FoodDetails,primary_key=True)
	gen_id = models.CharField(max_length=10)
	unit_price = models.PositiveSmallIntegerField()
	name = models.CharField(max_length=20)
	description = models.CharField(max_length=100)
	thumbnail_url = models.URLField(max_length=50)
	image_url = models.URLField(max_length=100)

	def __str__(self):
		return self.name

#Food Cart
class FoodCart(models.Model):
	cart = models.CharField(max_length=32,primary_key=True)
	customer = models.ForeignKey(CustomerCredentials)
	delivery_address = models.CharField(max_length=50)
	order_timestamp = models.DateTimeField(auto_now_add=True)
	bill = models.PositiveSmallIntegerField(default=32000)

	def save(self,*args,**kwargs):
		mix = self.customer.email + str(time.time())
		self.cart = hashlib.md5(mix).hexdigest()
		super(FoodCart,self).save(*args,**kwargs)	


# Food Orders
class FoodOrders(models.Model):
	forder = models.AutoField(primary_key=True)
	cart_id = models.ForeignKey(FoodCart)
	food_id = models.ForeignKey(FoodDetails)
	quantity = models.PositiveSmallIntegerField()
	order_status = models.CharField(max_length=2)


#Laundry Orders
class LaundryOrders(models.Model):
	order_id = models.AutoField(primary_key=True)
	customer_id = models.ForeignKey(CustomerCredentials)

#Bakery Orders
class BakeryOrders(models.Model):
	order_id = models.AutoField(primary_key=True)
	customer_id = models.ForeignKey(CustomerCredentials)
	greetings = models.CharField(max_length=100)

#Token detail
class CustomerToken(models.Model):
	token = models.CharField(primary_key=True,unique=True,max_length=64)
	user_id = models.ForeignKey(CustomerCredentials,on_delete=models.CASCADE)
	actions = models.TextField()
	#Saving
	def save(self,*args,**kwargs):
		mix = self.user_id.email + str(time.time())
		self.token = hashlib.sha224(mix).hexdigest()
		super(CustomerToken,self).save(*args,**kwargs)



