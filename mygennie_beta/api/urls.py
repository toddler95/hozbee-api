from django.conf.urls import url
import views

urlpatterns = [
    url(r'^customers/plogin/$', views.prelogin),
    url(r'^customers/login/$', views.login),
    url(r'^customers/logout/$', views.logout),
    url(r'^customers/cart/(?P<cart_id>[\w\-]+)/$', views.cart),
    url(r'^customers/order/([0-9]{5})/$', views.orderdetails),
    url(r'^customers/catalogue/', views.catalogue),
    url(r'^customers/placeorder/', views.placeorder),


    url(r'^test/$', views.json_test),


    url(r'^customers/$', views.customer_list),
    url(r'^catalogue/$', views.catalogue),

]
